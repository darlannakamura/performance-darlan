<?php 

class Projeto{
	private $id;
	private $nome;
	private $descricao;
	private $imagens;
	private $data;

	public function __construct(string $nome, string $descricao, array $imagens){
		$this->nome = $nome;
		$this->descricao = $descricao;
		$this->imagens = $imagens;
	}

	public function getID(){ return $this->id; }
	public function getNome(){ return $this->nome; }
	public function getDescricao(){ return $this->descricao; }
	public function getImagens(){ return $this->imagens; } 
	public function getData(){ return $this->data; } 
}

?>