<?php 
/**
 *
 * $slug - apelido - em alguma cada superior deve haver algum método para preenchimento do slug.
 * $imagens - notícia possui um array de imagens. Sendo o array composto por nenhuma imagem ou por N.
 */



class Noticia{
	private $id;
	private $titulo;
	private $descricao;
	private $data;
	private $slug; 
	private $imagens; 

	public function __construct(string $titulo, string $descricao, string $slug, array $imagens){
		$this->titulo = $titulo;
		$this->descricao = $descricao;
		$this->slug = $slug;
		$this->imagens = $imagens;
	}

	public function getID(){ return $this->id; }
	public function getTitulo(){ return $this->titulo; }
	public function getImagens(){ return $this->imagens; }
	public function getDescricao(){ return $this->descricao; }
	public function getSlug(){ return $this->slug; }
	public function getData(){ return $this->data; }
}

?>