<?php 

class Membro{
	private $id;
	private $nome;
	private $cargo;
	private $imagem;

	public function __construct(string $nome, string $cargo, Imagem $imagem){
		$this->nome = $nome;
		$this->cargo = $cargo;
		$this->imagem = $imagem;
	}

	public function getID(){ return $this->id; }
	public function getNome(){ return $this->nome; }
	public function getCargo(){ return $this->cargo; }
	public function getImagem(){ return $this->imagem; } 
}

?>