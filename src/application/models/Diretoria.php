<?php 
/* 
	getMembros() -  retorna os membros pertecentes à diretoria.
 */

class Diretoria{
	private $id;
	private $nome;
	private $descricao;
	private $membros;

	public function __construct(string $nome, string $descricao){
		$this->nome = $nome;
		$this->descricao = $descricao;
	}

	public function getID(){ return $this->id; }
	public function getNome(){ return $this->nome; }
	public function getDescricao(){ return $this->descricao; }
	public function getMembros(){ return $this->membros; }
}

?>