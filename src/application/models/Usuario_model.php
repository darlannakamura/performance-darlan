<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function num_rows($filtros = array()){
		
		if(isset($filtros['nome']) && $filtros['nome'] != ''){
			$this->db->like('usuario.nome', $filtros['nome']);
		}
		return $this->db->get('usuario')->num_rows();
	}

	public function listar_usuarios_paginados_com_filtro($filtros = array(), $inicio = 0){
		$this->db->join('imagem', 'usuario.imagem_id = imagem.id', 'INNER');
		if(!isset($filtros['attribute'])){
			$filtros['attribute'] = 'nome';
		} 
		if(!isset($filtros['order_by'])){
			$filtros['order_by'] = 'ASC';
		}
		if(!isset($filtros['quantidade'])){
			$filtros['quantidade'] = 10;
		}

		$this->db->order_by($filtros['attribute'], $filtros['order_by']);
		$this->db->limit($filtros['quantidade'], $inicio);
		
		if(isset($filtros['nome']) && $filtros['nome'] != ''){
			$this->db->like('usuario.nome', $filtros['nome']);
		}
		
		$this->db->select('usuario.*, imagem.nome as imagem');

		return $this->db->get('usuario')->result();

	}

	

  	public function update($email, $data){
  		
  		$this->db->where('email', $email);  
		return $this->db->update('usuario', $data);
  	}

  	public function delete($email){
		$this->db->where('email', $email);
		return $this->db->delete('usuario');
	}

}