<?php 

class Servico{
	private $id;
	private $nome;
	private $descricao;

	public function __construct(string $nome, string $descricao){
		$this->nome = $nome;
		$this->descricao = $descricao;
	}

	public function getID(){ return $this->id; }
	public function getNome(){ return $this->nome; }
	public function getDescricao(){ return $this->descricao; }
}

?>