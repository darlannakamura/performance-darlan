<?php 
class Contato{
	private $id;
	private $nome;
	private $descricao;
	private $icone;

	public function __construct(string $nome, string $descricao, string $icone){
		$this->nome = $nome;
		$this->descricao = $descricao;
		$this->icone = $icone;
	}

	public function getID(){ return $this->id; }
	public function getNome(){ return $this->nome; }
	public function getDescricao(){ return $this->descricao; }
	public function getIcone(){ return $this->icone; }
}

?>