<?php 

class Log{
	private $id;
	private $tabela;
	private $operacao;
	private $usuario;
	private $ip;
	private $data_registro;

	public function __construct(string $tabela, string $operacao, Usuario $usuario, $ip){
		$this->tabela = $tabela;
		$this->operacao = $operacao;
		$this->usuario = $usuario;
		$this->ip = $ip;
	}

	public function getID(){ return $this->id; }
	public function getTabela(){ return $this->tabela; }
	public function getOperacao(){ return $this->operacao; }
	public function getUsuario(){ return $this->usuario; }
	public function getIP(){ return $this->ip; }
	public function getData(){ return $this->data_registro; }
}

?>