<?php 

class Usuario{
	private $id;
	private $nome;
	private $email;
	private $senha;
	private $tipo_usuario;
	private $imagem;
	private $data_registro;

	public function __construct(string $email, string $senha, string $nome, int $tipo_usuario, Imagem $imagem){
		$this->email = $email;
		$this->senha = $senha;
		$this->nome = $nome;
		$this->tipo_usuario = $tipo_usuario;
		$this->imagem = $imagem;
	}

	public function getID(){ return $this->id; }
	public function getNome(){ return $this->nome; }
	public function getEmail(){ return $this->email; }
	public function getSenha(){ return $this->senha; }
	public function getTipoUsuario(){ return $this->tipo_usuario; }
	public function getImagem(){ return $this->imagem; }
	public function getDataRegistro(){ return $this->data_registro; }
	
}

?>